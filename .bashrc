[ -n "$PS1" ] && source ~/.bash_profile;

export HISTTIMEFORMAT="%d/%m/%y %T "

export GROOVY_HOME=/usr/local/opt/groovy/libexec
